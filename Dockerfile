FROM centos

MAINTAINER raed.louati@hotmail.fr

RUN mkdir /opt/tomcat/

WORKDIR /opt/tomcat
RUN curl -O https://downloads.apache.org/tomcat/tomcat-8/v8.5.73/bin/apache-tomcat-8.5.73.tar.gz
RUN tar xvfz apache*.tar.gz
RUN mv apache-tomcat-8.5.73/* /opt/tomcat/.
RUN yum -y install java
RUN java -version

WORKDIR /opt/tomcat/webapps
RUN curl -O -L https://bitbucket.org/raed0louati/project-test/src/master/build/welcome_latest.war

EXPOSE 8080

CMD ["/opt/tomcat/bin/catalina.sh", "run"]
